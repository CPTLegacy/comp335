"""
@author: Adam Goris 44908350
"""
from bottle import request, response
import uuid
from database import password_hash

# this variable MUST be used as the name for the cookie used by this application
COOKIE_NAME = 'sessionid'


def check_login(db, usernick, password):
    """returns True if password matches stored"""
    cursor = db.cursor()
    sql = "SELECT * FROM users WHERE nick = '" + usernick + "'"
    cursor.execute(sql)

    user = cursor.fetchone()

    if user:
        pword = user[1]
        return pword == password_hash(password)


def generate_session(db, usernick):
    """create a new session and add a cookie to the response object (bottle.response)
    user must be a valid user in the database, if not, return None
    There should only be one session per user at any time, if there
    is already a session active, use the existing sessionid in the cookie
    """

    cursor = db.cursor()
    sql = "SELECT * from users WHERE nick = '" + usernick + "'"
    cursor.execute(sql)

    user = cursor.fetchone()

    if user:

        cursor = db.cursor()
        sql = "SELECT * from sessions WHERE usernick = '" + usernick + "'"
        cursor.execute(sql)

        session = cursor.fetchone()

        if session:
            sessionid = session[0]
        else:
            sessionid = uuid.uuid4().hex

            cursor = db.cursor()
            sql = "INSERT INTO sessions VALUES (?, ?)"
            cursor.execute(sql, (sessionid, usernick,))
            db.commit()

        response.set_cookie(COOKIE_NAME, sessionid)
    else:
        sessionid = None

    return sessionid


def delete_session(db, usernick):
    """remove all session table entries for this user"""
    cursor = db.cursor();
    sql = "DELETE FROM sessions WHERE usernick='" + usernick + "'"
    cursor.execute(sql)

    response.delete_cookie(COOKIE_NAME)


def session_user(db):
    """try to
    retrieve the user from the sessions table
    return usernick or None if no valid session is present"""

    sessionid = request.get_cookie(COOKIE_NAME)

    if sessionid:
        cursor = db.cursor();
        sql = "SELECT usernick FROM sessions WHERE sessionid='" + sessionid + "'"
        cursor.execute(sql)

        user = cursor.fetchone()

        if user:
            return user[0]

    return None
