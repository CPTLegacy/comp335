__author__ = 'Adam Goris 44908350'

from bottle import Bottle, template, static_file, request, redirect
import interface
from users import check_login, generate_session, delete_session, session_user
import database
import gateway


app = Bottle()


@app.route('/')
def index(db):
    """ Main Page
            Displays the list of jobs using position_list from interface.
            position_list gives less information for description, allowing for 'Read More' link
        """

    exec("database")
    position_list = interface.position_list(db)

    info = {
        'title': 'Jobs!',
        'message': 'Welcome to Jobs',
        'pageName': 'Home',
        'pageLinkName': 'About',
        'linkPath': 'about',
        'jobs': position_list,
        'username': session_user(db),
    }

    return template('index', info)


@app.route('/about')
def about(db):
    """ About Page """

    info = {
        'title': 'Jobs!',
        'message': "Jobs is a new, exciting, job posting service like nothing you've seen before!",
        'pageName': 'About',
        'pageLinkName': 'Home',
        'linkPath': '',
        'jobs': "",
        'username': session_user(db),
    }

    return template('index', info)


@app.route('/positions/<posID>')
def rmore(db, posID):
    """ Page that is shown when a 'Read More' link is pressed.
        Has a dynamic URL based off the ID of the position that was clicked.
        Performs a call to the position.get function in interface to obtain all columns of
        required row.
    """

    exec("database")
    position = interface.position_get(db, posID)

    info = {
        'linkPath': '',
        'pageLinkName': 'Return',
        'title': 'Jobs!',
        'pageName': '',
        'id': position[0],
        'timestamp': position[1],
        'owner': position[2],
        'positionTitle': position[3],
        'location': position[4],
        'company': position[5],
        'description': position[6],
        'username': session_user(db),
    }

    return template('readmore', info)


@app.post('/login')
def handle_login(db):
    """ When a user enters their credentials in the login form,
        check the username and password entered is valid,
        and generate a session using their username,
        or produce an error message if it isn't a valid user
    """

    usernick = request.forms.get("nick")
    password = request.forms.get("password")

    # check login details
    valid = check_login(db, usernick, password)

    # create session
    if valid:
        generate_session(db, usernick)

        return redirect('/')

    else:
        position_list = interface.position_list(db)

        info = {
            'title': 'Jobs!',
            'message': 'Login Failed, please try again',
            'pageName': 'Failed Login',
            'pageLinkName': 'Home',
            'linkPath': '',
            'jobs': position_list,
            'username': session_user(db),
        }
        return template('incorrectLogin', info)


@app.post('/logout')
def handle_logout(db):
    usernick = session_user(db)
    """ delete_session also deletes 'sessionid' cookie """
    delete_session(db, usernick)

    return redirect('/')


@app.post('/post')
def handle_post(db):
    """ After the user submits their position, add it to the database """
    usernick = session_user(db)
    title = request.forms.get("title")
    location = request.forms.get("location")
    location = location + '<br>'
    company = request.forms.get("company")
    description = '<br> &emsp;'
    description = description + request.forms.get("description")

    interface.position_add(db, usernick, title, location, company, description)

    return redirect('/')


@app.route('/static/<filename:path>')
def static(filename):
    return static_file(filename=filename, root='static')


if __name__ == '__main__':

    from bottle.ext import sqlite
    from database import DATABASE_NAME
    # install the database plugin
    app.install(sqlite.Plugin(dbfile=DATABASE_NAME))
    app.run(debug=True, port=8019)
