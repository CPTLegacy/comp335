"""
Database Model interface for the COMP249 Web Application assignment

@author: Adam Goris 44908350
"""
import time

def position_list(db, limit=10):
    """Return a list of positions ordered by date
    db is a database connection
    return at most limit positions (default 10)

    Returns a list of tuples  (id, timestamp, owner, title, location, company, description)
    """

    cursor = db.cursor()
    sql = "SELECT id, timestamp, owner, title, location, company, SUBSTR(description, 0 , 200)" \
          " FROM positions ORDER BY timestamp DESC LIMIT'" + str(limit) + "'"
    cursor.execute(sql)

    return [row for row in cursor]


def position_get(db, id):
    """Return the details of the position with the given id
    or None if there is no position with this id

    Returns a tuple (id, timestamp, owner, title, location, company, description)

    """

    cursor = db.cursor()
    sql = "SELECT * FROM positions WHERE id = '" + str(id) + "'"
    cursor.execute(sql)

    topRow = cursor.fetchone()

    if not (topRow is None):
        topRow = list(topRow)
        return topRow
    else:
        return None


def position_add(db, usernick, title, location, company, description):
    """Add a new post to the database.
    The date of the post will be the current time and date.
    Only add the record if usernick matches an existing user

    Return True if the record was added, False if not."""

    cursor = db.cursor()
    sql = "SELECT * FROM users WHERE nick = '" + str(usernick) + "'"
    cursor.execute(sql)

    if [row[0] for row in cursor]:
        cursor = db.cursor()
        sql = "INSERT INTO positions (owner, title, location, company, description) VALUES (?, ?, ?, ?, ?)"
        cursor.execute(sql, (usernick, title, location, company, description,))
        db.commit()
        return True

    else:
        return False


