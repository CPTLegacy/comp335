__author__ = 'Adam Goris 44908350,  Michael Serretta 43276776'

from bottle import Bottle, template, static_file, request, redirect
import cart
import cartdatabase
import newcart

app = Bottle()


#go back to order button
@app.post('/order')
def goToOrder():
    restaurantID = request.forms.get("restaurantID")
    user = request.forms.get("user")
    print("back to ordering from the menu")
    return redirect('http://127.0.0.1:8012/' + restaurantID + "/" + user)


@app.route('/')
def index(db):
    exec("cartdatabase")

    item_list = cart.get_items(db)
    total = cart.get_total(db)

    info = {
        'title': 'Project Icarus',
        'pageName': 'Checkout',
        'message': 'Please enter your details:',
        'items': item_list,
        'total': total
    }

    return template('index', info)


@app.route('/cart/<cartCode>/<username>/<restaurantID>')
def carts(db, cartCode, username, restaurantID):
    print("incoming cartCode: " + cartCode)
    newcart.create_tables(db)
    """create cartList from cartCode"""
    cartList = createCartList(cartCode)

    newcart.downloadCart(db, cartList)

    item_list = cart.get_items(db)
    total = cart.get_total(db)

    info = {
        'title': 'Project Icarus',
        'pageName': 'Checkout',
        'message': 'Please enter your details:',
        'items': item_list,
        'total': total,
        'username': username,
        'errorMessage': 'Incorrect username/password',
        'restaurantID': restaurantID,
    }

    return template('index', info)


@app.post('/placeorder')
def index(db):
    username = request.forms.get('user')
    address = request.forms.get('address')
    phone = request.forms.get('phone')
    total = request.forms.get('total')
    restaurantID = request.forms.get('restaurantID')

    info = {
        'title': 'Project Icarus',
        'pageName': 'Completed',
        'message': 'Thank you for your order.',
        'address': address,
        'phone': phone,
        'total': total,
        'username': username,
        'errorMessage': 'Incorrect username/password',
        'restaurantID': restaurantID,
    }

    return template('orderplaced', info)


@app.post('/logout')
def handle_logout(db):
    return redirect('http://127.0.0.1:8010/logout')


@app.route('/static/<filename:path>')
def static(filename):
    return static_file(filename=filename, root='static')


def createCartList(cartCode):
    cartList = []
    dishIndex = []
    quantityIndex = []
    priceIndex = []
    for i, char in enumerate(cartCode):
        if char == '&':
            dishIndex.append(i+1)
        if char == ',':
            quantityIndex.append(i+1)
        if char == '$':
            priceIndex.append(i+1)
        if char == '!':
            dishIndex.append(i)

    for i, value in enumerate(priceIndex):
        dish = cartCode[dishIndex[i]:quantityIndex[i]-1]
        quantity = int(cartCode[quantityIndex[i]:priceIndex[i]-1])
        price = float(cartCode[priceIndex[i]:dishIndex[i+1]-1])
        total = quantity * price
        set = [dish, price, quantity, total]
        cartList.append(set)

    return cartList


if __name__ == '__main__':
    from bottle.ext import sqlite
    from cartdatabase import DATABASE_NAME
    # install the database plugin
    app.install(sqlite.Plugin(dbfile=DATABASE_NAME))
    app.run(debug=True, port=8013)
