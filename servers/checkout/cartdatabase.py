import sqlite3

# The database name
DATABASE_NAME = 'cart.db'


def create_tables(db):
    """Create and initialise the database tables
    This will have the effect of overwriting any existing
    data."""

    sql = """
DROP TABLE IF EXISTS carts;
CREATE TABLE carts (
       itemName text,
       price decimal(6, 2)
);
"""

    db.executescript(sql)
    db.commit()


def sample_data(db):
    """Generate some sample data for testing the web
    application. Erases any existing data in the
    database
    Returns the list of users and the list of positions
    that are inserted into the database"""

    # Setup carts table
    #       itemName, price
    carts = [('Blandit', '9.99'),
             ('Eget', '5.99'),
             ('Porttitor', '11.99')]

    cursor = db.cursor()
    cursor.execute("DELETE FROM carts")

    for itemName, price in carts:
        sql = "INSERT INTO carts (itemName, price) VALUES (?, ?)"
        cursor.execute(sql, (itemName, price))

    db.commit()

    return carts


if __name__ == '__main__':
    # if we call this script directly, create the database and make sample data
    db = sqlite3.connect(DATABASE_NAME)
    create_tables(db)
    sample_data(db)