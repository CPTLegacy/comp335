import sqlite3

# The database name
DATABASE_NAME = 'cart.db'


def create_tables(db):
    """Create and initialise the database tables
    This will have the effect of overwriting any existing
    data."""

    sql = """
DROP TABLE IF EXISTS carts;
CREATE TABLE carts (
       itemName text,
       price decimal(6, 2),
       quantity decimal(6, 2),
       total decimal(6, 2)
);
"""

    db.executescript(sql)
    db.commit()


def downloadCart(db, cart):
    """store cart information into the database"""

    # Setup carts table
    #       itemName, price, quantity, total

    cursor = db.cursor()
    cursor.execute("DELETE FROM carts")

    for itemName, price, quantity, total in cart:
        sql = "INSERT INTO carts (itemName, price, quantity, total) VALUES (?, ?, ? ,?)"
        cursor.execute(sql, (itemName, price, quantity, total))

    db.commit()

    return cart


if __name__ == '__main__':
    # if we call this script directly, create the database and make sample data
    db = sqlite3.connect(DATABASE_NAME)
    create_tables(db)
    sample_data(db)