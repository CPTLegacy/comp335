def get_items(db):
    cursor = db.cursor()
    sql = "SELECT * FROM carts"
    cursor.execute(sql)

    return [row for row in cursor]


def get_total(db):
    cursor = db.cursor()
    sql = "SELECT total FROM carts"
    cursor.execute(sql)

    result = cursor.fetchall()
    total = 0
    for price in result:
        total = total + price[0]

    return total


