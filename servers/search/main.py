__author__ = 'Adam Goris 44908350'

from bottle import Bottle, template, static_file, request, redirect
import search
import tcpsocket
import restaurants
import socket


app = Bottle()

@app.route('/<username>')
def index(db, username):
    exec("restaurants")
    user = username
    restaurant_list = search.restaurant_list(db)

    info = {
        'title': 'Project Icarus',
        'message': 'Showing results for restaurants:',
        'pageName': 'Restaurants',
        'restaurants': restaurant_list,
        'username': username,
        'errorMessage': 'Incorrect username/password',
    }

    return template('index', info)


@app.post('/logout')
def handle_logout(db):
    return redirect('http://127.0.0.1:8010/logout')


@app.route('/static/<filename:path>')
def static(filename):
    return static_file(filename=filename, root='static')


@app.post('/menu')
def goToMenu():
    restaurantID = request.forms.get("restaurant")
    user = request.forms.get("user")
    print("restaurantID: " + restaurantID + "/" + user)
    return redirect('http://127.0.0.1:8012/' + restaurantID + "/" + user)


if __name__ == '__main__':
    from bottle.ext import sqlite
    from restaurants import DATABASE_NAME
    # install the database plugin
    app.install(sqlite.Plugin(dbfile=DATABASE_NAME))
    app.run(debug=True, port=8011)
