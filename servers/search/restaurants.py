import sqlite3
import hashlib

# The database name
DATABASE_NAME = 'restaurants.db'


def create_tables(db):
    """Create and initialise the database tables
    This will have the effect of overwriting any existing
    data."""

    sql = """
DROP TABLE IF EXISTS owners;
CREATE TABLE owners (
       userID int unique primary key,
       firstName text,
       lastName text,
       password text,
       avatar text
);

DROP TABLE IF EXISTS restaurants;
CREATE TABLE restaurants (
        restaurantID int unique primary key,
        restaurantName text,
        cuisine text,
        description text,
        address text,
        ownerID int,
        FOREIGN KEY(ownerID) REFERENCES owners(userID)
);
"""

    db.executescript(sql)
    db.commit()


def sample_data(db):
    """Generate some sample data for testing the web
    application. Erases any existing data in the
    database
    Returns the list of users and the list of positions
    that are inserted into the database"""

    # Setup Owners table
    #       userID, firstName, lastName, password, avatar
    owners = [('0', 'Bob', 'Loblaw', 'bob', 'http://robohash.org/bob'),
              ('1', 'Jim', 'Bership', 'jim', 'http://robohash.org/jim'),
              ('2', 'Sophia', 'Shock', 'sop', 'http://robohash.org/mary')]
    #  Avatars by Robohash.org

    cursor = db.cursor()

    # create one entry for each user
    for userID, firstName, lastName, password, avatar in owners:
        sql = "INSERT INTO owners (userID, firstName, lastName, password, avatar) VALUES (?, ?, ?, ?, ?)"
        cursor.execute(sql, (userID, firstName, lastName, password, avatar))

    # Setup restaurants table
    restaurants = [('0', "Bob's Kebobs", 'Afgan Shish-Kebobs', "The finest Afgan shish-kebobs on the planet.", "123 Kebab St, Kebabland", '0'),
                   ('1', "Jim's Chinese Restaurant", 'Chinese', "Traditional Chinese restaurant.", "50 Fake St, Rivertown", '1'),
                   ('2', "Sophia's Lamb", 'Barbecue', "Great flavours and wide variety of barbecued lamb.", "17 Sheep St, Baatown", '2')]

    cursor = db.cursor()

    # create one entry for each restaurant
    for restaurantID, restaurantName, cuisine, description, address, ownerID in restaurants:
        sql = "INSERT INTO restaurants (restaurantID, restaurantName, cuisine, description, address, " \
              "ownerID) VALUES (?, ?, ?, ?, ?, ?)"
        cursor.execute(sql, (restaurantID, restaurantName, cuisine, description, address, ownerID))

    db.commit()
    cursor.execute("DELETE FROM owners")
    cursor.execute("DELETE FROM restaurants")

    return owners, restaurants


if __name__ == '__main__':
    # if we call this script directly, create the database and make sample data
    db = sqlite3.connect(DATABASE_NAME)
    create_tables(db)
    sample_data(db)