def restaurant_list(db):
    """Return a list of positions ordered by date
    db is a database connection
    Returns a list of tuples (restaurantID, restaurantName, cuisine, description, address, ownerID)
    """

    cursor = db.cursor()
    sql = "SELECT * FROM restaurants"
    cursor.execute(sql)

    return [row for row in cursor]


def restaurant_get(db, id):
    """Return the details of the restaurant with the given id
    or None if there is no position with this id

    Returns a tuples (restaurantID, restaurantName, cuisine, description, address, ownerID)
    """

    cursor = db.cursor()
    sql = "SELECT * FROM restaurants WHERE restaurantID = '" + str(id) + "'"
    cursor.execute(sql)

    topRow = cursor.fetchone()

    if not (topRow is None):
        topRow = list(topRow)
        return topRow
    else:
        return None


def restaurant_add(db, restaurantname, cuisine, description, address, ownerid):
    """Add a new post to the database.
    The date of the post will be the current time and date.
    Only add the record if usernick matches an existing user

    Return True if the record was added, False if not."""

    cursor = db.cursor()
    sql = "SELECT * FROM users WHERE nick = '" + str(ownerid) + "'"
    cursor.execute(sql)

    if [row[0] for row in cursor]:
        cursor = db.cursor()
        sql = "INSERT INTO positions (restaurantName, cuisine, description, address, ownerID) VALUES (?, ?, ?, ?, ?)"
        cursor.execute(sql, (restaurantname, cuisine, description, address, ownerid,))
        db.commit()
        return True

    else:
        return False


