from bottle import Bottle, template, request, redirect, static_file
from authentication import check_login, session_user, generate_session, delete_session
import authenticationDatabase

app = Bottle()


@app.route('/')
def index(db):

    exec("authenticationDatabase")

    info = {
        'title': 'Project Icarus',
        'pageName': 'Home',
        'message': 'Welcome to the greatest food ordering site the world has ever seen',
        'username': session_user(db),
        'errorMessage': '',
    }

    return template('index', info)


@app.post('/login')
def handle_login(db):
    """ When a user enters their credentials in the login form,
        check the username and password entered is valid,
        and generate a session using their username,
        or produce an error message if it isn't a valid user
    """

    usernick = request.forms.get("nick")
    password = request.forms.get("password")

    # check login details
    valid = check_login(db, usernick, password)

    # create session
    if valid:
        generate_session(db, usernick)

        return redirect('http://127.0.0.1:8011/' + usernick)

    else:
        info = {
            'title': 'Project Icarus',
            'pageName': 'Home',
            'message': 'Welcome to the greatest food ordering site the world has ever seen',
            'username': session_user(db),
            'errorMessage': 'Incorrect username/password',
        }
        return template('index', info)


@app.post('/logout')
def handle_logout(db):
    usernick = session_user(db)
    """ delete_session also deletes 'sessionid' cookie """
    delete_session(db, usernick)

    return redirect('/')


@app.route('/logout')
def handle_logout(db):
    usernick = session_user(db)
    """ delete_session also deletes 'sessionid' cookie """
    delete_session(db, usernick)

    return redirect('/')


@app.route('/static/<filename:path>')
def static(filename):
    return static_file(filename=filename, root='static')


if __name__ == '__main__':
    from bottle.ext import sqlite
    from authenticationDatabase import DATABASE_NAME
    # install the database plugin
    app.install(sqlite.Plugin(dbfile=DATABASE_NAME))
    app.run(debug=True, port=8010)