import sqlite3
import hashlib

# The database name
DATABASE_NAME = 'authentication.db'


def password_hash(password):
    """Return a one-way hashed version of the password suitable for
    storage in the database"""

    return hashlib.sha1(password.encode()).hexdigest()


def create_tables(db):
    """Create and initialise the database tables
    This will have the effect of overwriting any existing
    data."""

    sql = """
DROP TABLE IF EXISTS users;
CREATE TABLE users (
       nick text unique primary key,
       password text,
       avatar text
);

DROP TABLE IF EXISTS sessions;
CREATE TABLE sessions (
        sessionid text unique primary key,
        usernick text,
        FOREIGN KEY(usernick) REFERENCES users(nick)
);
"""

    db.executescript(sql)
    db.commit()


def sample_data(db):
    """Generate some sample data for testing the web
    application. Erases any existing data in the
    database
    Returns the list of users and the list of positions
    that are inserted into the database"""

    #  pass,   nick             avatar
    users = [('bob', 'Bobalooba', 'http://robohash.org/bob'),
             ('jim', 'Jimbulator', 'http://robohash.org/jim'),
             ('mary', 'Contrary', 'http://robohash.org/mary'),
             ('jb', 'Bean', 'http://robohash.org/jb'),
             ('mandible', 'Mandible', 'http://robohash.org/mandible'),
             ('bar', 'Barfoo', 'http://robohash.org/bar')]
    #  Avatars by Robohash.org

    cursor = db.cursor()
    cursor.execute("DELETE FROM users")

    # create one entry for each user
    for password, nick, avatar in users:
        sql = "INSERT INTO users (nick, password, avatar) VALUES (?, ?, ?)"
        cursor.execute(sql, (nick, password_hash(password), avatar))

    db.commit()

    return users


if __name__ == '__main__':
    # if we call this script directly, create the database and make sample data
    db = sqlite3.connect(DATABASE_NAME)
    create_tables(db)
    sample_data(db)
