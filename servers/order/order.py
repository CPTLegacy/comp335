def menu_entree_list(db, id):
    """Return a list of items of a given restaurant
    db is a database connection
    Returns a list of tuples (item, price, category)
    """

    cursor = db.cursor()
    sql = "SELECT itemID, restaurantID, item, price FROM items WHERE  restaurantID = '" + str(id) + "' AND category = 'entree'"
    cursor.execute(sql)

    return [row for row in cursor]


def menu_main_list(db, id):
    """Return a list of items of a given restaurant
    db is a database connection
    Returns a list of tuples (item, price, category)
    """

    cursor = db.cursor()
    sql = "SELECT itemID, restaurantID, item, price FROM items WHERE  restaurantID = '" + str(id) + "' AND category = 'main'"
    cursor.execute(sql)

    return [row for row in cursor]


def menu_dessert_list(db, id):
    """Return a list of items of a given restaurant
    db is a database connection
    Returns a list of tuples (item, price, category)
    """

    cursor = db.cursor()
    sql = "SELECT itemID, restaurantID, item, price FROM items WHERE  restaurantID = '" + str(id) + "' AND category = 'dessert'"
    cursor.execute(sql)

    return [row for row in cursor]


def restaurant_name(db, id):
    """Return name of the restaurant given restaurantID"""
    cursor = db.cursor()
    sql = "SELECT restaurantName FROM restaurants WHERE  restaurantID = '" + str(id) + "'"
    cursor.execute(sql)

    return [row[0] for row in cursor]


def add_to_cart(db, itemid, restaurantid, price, dish):
    cursor = db.cursor()
    sql = "INSERT INTO carts (itemID, restaurantID, price, dish) VALUES (?, ?, ?, ?)"
    cursor.execute(sql, (itemid, restaurantid, price, dish))
    db.commit()


def cart_list(db):
    cursor = db.cursor()
    sql = "SELECT * FROM carts"
    cursor.execute(sql)

    return [row for row in cursor]


def get_item(db, itemid, restaurantid):
    """ Return item information given restaurantID and itemID """
    cursor = db.cursor()
    sql = "SELECT item, price FROM items WHERE restaurantID = '" + str(restaurantid) + "' AND itemID = '" + str(itemid) + "'"
    cursor.execute(sql)

    return [row for row in cursor]


def item_add(db, item, price, category, restaurantid):
    """Add a new item to the database. For business owners
    Only add the record if usernick matches an existing user

    Return True if the record was added, False if not."""

    cursor = db.cursor()
    sql = "INSERT INTO items (restaurantid, item, price, category) VALUES (?, ?, ?, ?)"
    cursor.execute(sql, (restaurantid, item, price, category))
    db.commit()




