import sqlite3

# The database name
DATABASE_NAME = 'menus.db'


def create_tables(db):
    """Create and initialise the database tables
    This will have the effect of overwriting any existing
    data."""

    sql = """
DROP TABLE IF EXISTS restaurants;
CREATE TABLE restaurants (
        restaurantID int unique primary key,
        restaurantName text,
        cuisine text,
        description text,
        address text
);

DROP TABLE IF EXISTS items;
CREATE TABLE items (
    itemID int unique primary key,
    restaurantID int,
    item text,
    price decimal(6, 2),
    category text,
    FOREIGN KEY(restaurantID) REFERENCES restaurants(restaurantID)  
);

DROP TABLE IF EXISTS carts;
CREATE TABLE carts (
    itemID int,
    restaurantID int,
    price decimal(6, 2),
    dish text,
    FOREIGN KEY(itemID) REFERENCES items(itemID) 
    FOREIGN KEY(restaurantID) REFERENCES restaurants(restaurantID) 
);
"""

    db.executescript(sql)
    db.commit()


def sample_data(db):
    """Generate some sample data for testing the web
    application. Erases any existing data in the
    database
    Returns the list of users and the list of positions
    that are inserted into the database"""

    # Setup restaurants table
    restaurants = [('0', "Bob's Kebobs", 'Afgan Shish-Kebobs', "The finest Afgan shish-kebobs on the planet.", "123 Kebab St, Kebabland"),
                   ('1', "Jim's Chinese Restaurant", 'Chinese', "Traditional Chinese restaurant.", "50 Fake St, Rivertown"),
                   ('2', "Sophia's Lamb", 'Barbecue', "Great flavours and wide variety of barbecued lamb.", "17 Sheep St, Baatown")]

    cursor = db.cursor()

    # create one entry for each restaurant
    for restaurantID, restaurantName, cuisine, description, address in restaurants:
        sql = "INSERT INTO restaurants (restaurantID, restaurantName, cuisine, description, address) " \
              "VALUES (?, ?, ?, ?, ?)"
        cursor.execute(sql, (restaurantID, restaurantName, cuisine, description, address))

    # Setup item table
    items = [('0', '0', 'Blandit', '9.99', 'entree'),
             ('1', '0', 'Eget', '5.99', 'entree'),
             ('2', '0', 'Porttitor', '11.99', 'entree'),
             ('3', '0', 'Efficitur', '10.99', 'entree'),
             ('4', '0', 'Donec', '19.99', 'main'),
             ('5', '0', 'Lacus', '25.99', 'main'),
             ('6', '0', 'Nulla', '18.99', 'main'),
             ('7', '0', 'Pellentesque', '27.99', 'main'),
             ('8', '0', 'Duis', '30.99', 'main'),
             ('9', '0', 'Aliquam', '15.99', 'main'),
             ('10', '0', 'Scelerisque', '17.99', 'main'),
             ('11', '0', 'Phasellus', '13.99', 'dessert'),
             ('12', '0', 'Bibendum', '14.99', 'dessert')]

    cursor = db.cursor()

    # create one entry for each item
    for itemID, restaurantID, item, price, category in items:
        sql = "INSERT INTO items (itemID, restaurantID, item, price, category) VALUES (?, ?, ?, ?, ?)"
        cursor.execute(sql, (itemID, restaurantID, item, price, category))

    db.commit()
    cursor.execute("DELETE FROM restaurants")
    cursor.execute("DELETE FROM items")

    return restaurants, items


if __name__ == '__main__':
    # if we call this script directly, create the database and make sample data
    db = sqlite3.connect(DATABASE_NAME)
    create_tables(db)
    sample_data(db)