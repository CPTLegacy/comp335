__author__ = 'Adam Goris 44908350, Michael Serretta 43276776'

from bottle import Bottle, template, static_file, request, redirect
import order
import menus

app = Bottle()


@app.route('/')
def index(db):

    exec("menus")
    entree_list = order.menu_entree_list(db, '0')
    main_list = order.menu_main_list(db, '0')
    dessert_list = order.menu_dessert_list(db, '0')
    restaurant_name = order.restaurant_name(db, '0')

    items = order.cart_list(db)
    cart_items = []
    for item in items:
        cart_items.append(order.get_item(db, item[0], item[1]))

    info = {
        'title': 'Project Icarus',
        'message': 'Select Items to Add to Cart:',
        'pageName': 'Make Your Order',
        'restaurantName': restaurant_name,
        'entrees': entree_list,
        'mains': main_list,
        'desserts': dessert_list,
        'cart': cart_items,
    }

    return template('index', info)


@app.route('/<restaurantID>/<username>')
def indexActual(db,restaurantID,username):

    exec("menus")
    entree_list = order.menu_entree_list(db, restaurantID)
    main_list = order.menu_main_list(db, restaurantID)
    dessert_list = order.menu_dessert_list(db, restaurantID)
    restaurant_name = order.restaurant_name(db, restaurantID)

    items = order.cart_list(db)
    cart_items = []
    for item in items:
        cart_items.append(order.get_item(db, item[0], item[1]))

    info = {
        'title': 'Project Icarus',
        'message': 'Select Items to Add to Cart:',
        'pageName': 'Make Your Order',
        'restaurantName': restaurant_name,
        'entrees': entree_list,
        'mains': main_list,
        'desserts': dessert_list,
        'cart': cart_items,
        'username': username,
        'errorMessage': 'Incorrect username/password',
        'restaurantID': restaurantID
    }

    return template('index', info)


@app.post('/logout')
def handle_logout(db):
    return redirect('http://127.0.0.1:8010/logout')


#go back to search restaurants button
@app.post('/restaurants')
def goToSearch():
    user = request.forms.get("user")
    print("back to searching restaurant")
    return redirect('http://127.0.0.1:8011/' + user)

#go to cart
@app.post('/cart')
def goToCart(db):
    print("continuing to cart and checkout \n order:\n")
    items = order.cart_list(db)
    user = request.forms.get("user")
    restaurantID = request.forms.get("restaurantID")
    cartList = []
    cartCode = ""
    for item in items:
        #print(str(item[0]) + "$" + str(item[2]))
        itemFound = False
        print(item[3])
        for dish in cartList:
            if item[3] == dish[0]:
                dish[1] += 1
                itemFound = True
        if itemFound == False:
            set = [item[3], 1, item[2]]
            cartList.append(set)
    for dish in cartList:
        cartCode += "&"+dish[0]+","+str(dish[1])+"$"+str(dish[2])
    cartCode += "!/"

    print("cartCode: "+cartCode)
        #print("\n")


    return redirect('http://127.0.0.1:8013/cart/'+cartCode + user + "/" + restaurantID)


@app.post('/addtocart')
def cart(db):
    item_id = request.forms.get('item')
    restaurant_id = request.forms.get('restaurant')
    price = request.forms.get('price')
    dish = request.forms.get('dish')
    user = request.forms.get('user')
    restaurantID = request.forms.get('restaurantID')
    print(dish)
    order.add_to_cart(db, item_id, restaurant_id, price, dish)

    return redirect('/' + restaurantID + '/' + user)


@app.route('/static/<filename:path>')
def static(filename):
    return static_file(filename=filename, root='static')


if __name__ == '__main__':
    from bottle.ext import sqlite
    from menus import DATABASE_NAME
    # install the database plugin
    app.install(sqlite.Plugin(dbfile=DATABASE_NAME))
    app.run(debug=True, port=8012)
